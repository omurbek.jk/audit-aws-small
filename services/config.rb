coreo_aws_rule "iam-missing-password-policy" do
  action :define
  service :iam
  link "https://kb.securestate.vmware.com/aws-iam-missing-password-policy.html"
  display_name "Password policy doesn't exist"
  description "There currently isn't a password policy to require a certain password length, password expiration, prevent password reuse, and more."
  category "Access"
  suggested_action "Configure a strong password policy for your users to ensure that passwords expire, aren't reused, have a certain length, require certain characters, and more."
  level "High"
  meta_nist_171_id "3.5.7"
  objectives ["account_password_policy"]
  audit_objects ["object"]
  operators ["=="]
  raise_when [nil]
  id_map "static.password_policy"
  meta_compliance (
                      [
                          { "name" => "nist-sp800-171", "version" => "r1", "requirement" => "3.5.7" }
                      ]
                  )
  meta_rule_query <<~QUERY
  {
    var(func: has(user)) {
      name as object_id
    }
    pwp as var(func: <%= filter['password_policy'] %>) { }
    query_2(func: uid(pwp)) @cascade {
      <%= default_predicates %>
      minimum_password_length
      require_uppercase_characters
      require_lowercase_characters
      require_numbers
      require_symbols
      expire_passwords
      allow_users_to_change_password
    }
    query_1(func: eq(val(name), "<root_account>")) {
      <%= default_predicates %>
    }
  }
  QUERY
  meta_rule_node_triggers({
                              'password_policy' => []
                          })
end

coreo_aws_rule "ec2-vpc-flow-logs" do
  action :define
  service :user
  category "Audit"
  link "https://kb.securestate.vmware.com/aws-ec2-vpc-flow-logs.html"
  display_name "Ensure VPC flow logging is enabled in all VPCs (Scored)"
  suggested_action "VPC Flow Logs be enabled for packet 'Rejects' for VPCs."
  description "VPC Flow Logs is a feature that enables you to capture information about the IP traffic going to and from network interfaces in your VPC. After you've created a flow log, you can view and retrieve its data in Amazon CloudWatch Logs."
  level "Low"
  meta_cis_id "4.3"
  meta_cis_scored "true"
  meta_cis_level "1"
  meta_nist_171_id "3.13.1, 3.13.6"
  objectives [""]
  audit_objects [""]
  operators [""]
  raise_when [true]
  id_map "static.no_op"
  meta_compliance (
                      [
                          { "name" => "nist-sp800-171", "version" => "r1", "requirement" => "3.13.1" },
                          { "name" => "nist-sp800-171", "version" => "r1", "requirement" => "3.13.6" },
                          { "name" => "cis-aws-foundations-benchmark", "version" => "1.2.0", "requirement" => "4.3" }
                      ]
                  )
  meta_rule_query <<~QUERY
  {
    vpcs as var(func: <%= filter['vpc'] %>) @cascade {
      fl as relates_to @filter(<%= filter['flow_log'] %>) {
        fls as flow_log_status
      }
    }
    v as var(func: uid(vpcs)) @cascade {
      relates_to @filter(uid(fl) AND eq(val(fls), "ACTIVE"))
    }
    query(func: <%= filter['vpc'] %>) @filter(NOT uid(v)) {
      <%= default_predicates %>
    }
  }
  QUERY
  meta_rule_visualize <<~QUERY
  {
    query(func: uid(<%= violation_uid %>)){
      <%= default_predicates %>
      relates_to @filter(NOT has(flow_log)) {
        <%= default_predicates %>
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                              'vpc' => [],
                              'flow_log' => ['flow_log_status']
                          })
end

coreo_aws_rule "s3-authenticatedusers-access" do
  action :define
  service :s3
  link "https://kb.securestate.vmware.com/aws-s3-authenticatedusers-access.html"
  display_name "Any Authenticated User has access"
  description "Bucket policy gives any authenticated user some sort of access to this s3 bucket"
  category "Security"
  suggested_action "Remove or modify the bucket policy that enables any authenticated user access."
  level "High"
  meta_nist_171_id "3.1.3"
  objectives     ["buckets", "bucket_policy"]
  call_modifiers [{}, {:bucket => "buckets.name"}]
  audit_objects ["", "object.policy"]
  formulas      ["", "jmespath.Statement[?Effect == 'Allow' && !Condition].Principal"]
  operators     ["", "=~"]
  raise_when    ["", /"AWS":\s*"\*"/]
  id_map "modifiers.bucket"
  meta_compliance (
                      [
                          { "name" => "nist-sp800-171", "version" => "r1", "requirement" => "3.1.3" }
                      ]
                  )
  meta_rule_query <<~QUERY
  {
    buckets as var(func: <%= filter['bucket'] %>) { }
    policies as var(func:<%= filter['policy'] %>) { }
    statements as var(func: <%= filter['policy_statement'] %>)  @cascade {
      p as principal_aws
      e as effect
    }
    unrestricted_statements as var(func: uid(statements)) @filter(eq(val(p), "*") AND eq(val(e), "Allow") AND NOT has(condition)){ }
    open_buckets as var(func: uid(buckets)) @cascade {
      relates_to @filter(uid(policies)) {
        relates_to @filter(uid(unrestricted_statements)) { }
      }
    }
    query(func: uid(open_buckets)) {
      <%= default_predicates %>
      relates_to @filter(uid(policies)) {
        <%= default_predicates %>
        relates_to @filter(uid(unrestricted_statements)) {
          <%= default_predicates %>
          principal_aws
          action
          not_action
          effect
        }
      }
    }
  }
  QUERY
  meta_rule_visualize <<~QUERY
  {
    policies as var(func:<%= filter['policy'] %>) { }
    statements as var(func: <%= filter['policy_statement'] %>)  @cascade {
      p as principal_aws
      e as effect
    }
    bacls as var(func: has(bucket_acl)) { }
    bacl_grants as var(func: has(bucket_acl_grant)) @cascade { }
    grantees as var(func: has(grantee)) { }
    owners as var(func: has(owner)) { }
    unrestricted_statements as var(func: uid(statements)) @filter(eq(val(p), "*") AND eq(val(e), "Allow") AND NOT has(condition)){ }
    query(func: uid(<%= violation_uid %>)) {
      <%= default_predicates %>
      relates_to @filter(uid(policies, bacls, owners)) {
        <%= default_predicates %>
        relates_to @filter(uid(unrestricted_statements, bacl_grants)) {
          <%= default_predicates %>
          principal_aws
          principal
          action
          not_action
          effect
          permission
          display_name
          relates_to @filter(uid(grantees)) {
            <%= default_predicates %>
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers({
                              'bucket' => [],
                              'policy' => [],
                              'policy_statement' => ['principal', 'effect', 'condition']
                          })
end

coreo_aws_rule "cloudtrail-service-disabled" do
  action :define
  service :cloudtrail
  link "https://kb.securestate.vmware.com/aws-cloudtrail-service-disabled.html"
  display_name "Cloudtrail Service is Disabled"
  description "CloudTrail logging is not enabled for this region. It should be enabled."
  category "Audit"
  suggested_action "Enable CloudTrail logs for each region."
  level "Low"
  meta_cis_id "2.1"
  meta_cis_scored "true"
  meta_cis_level "1"
  meta_nist_171_id "3.1.12, 3.3.7, 3.3.1, 3.3.2, 3.3.4"
  objectives ["trails"]
  formulas ["count"]
  audit_objects ["object.trail_list"]
  operators ["=="]
  raise_when [0]
  id_map "stack.current_region"
  meta_compliance (
                      [
                          { "name" => "nist-sp800-171", "version" => "r1", "requirement" => "3.1.12" },
                          { "name" => "nist-sp800-171", "version" => "r1", "requirement" => "3.3.7" },
                          { "name" => "nist-sp800-171", "version" => "r1", "requirement" => "3.3.1" },
                          { "name" => "nist-sp800-171", "version" => "r1", "requirement" => "3.3.2" },
                          { "name" => "nist-sp800-171", "version" => "r1", "requirement" => "3.3.4" },
                          { "name" => "cis-aws-foundations-benchmark", "version" => "1.2.0", "requirement" => "2.1" }
                      ]
                  )
  meta_rule_query <<~QUERY
  {
    var(func: has(user)) {
      name as object_id
    }
    query_2(func: <%= filter['trail'] %>) {
      <%= default_predicates %>
    }
    query_1(func: eq(val(name), "<root_account>")) {
      <%= default_predicates %>
    }
  }
  QUERY
  meta_rule_node_triggers({
                              'trail' => []
                          })
end

coreo_aws_rule "administrative-policy-exposed-by-connected-ssh-credential" do
  action :define
  service :ec2
  link "https://kb.securestate.vmware.com/aws-connected-threats-ssh-credentials"
  display_name "Publicly routable instance shares ssh-key with administrative instances"
  description "A publicly routable and addressable ec2 instance has the same ssh key as an instance with an administrative policy."
  category "Security"
  suggested_action "Generate distinct ssh keys per subnet or ec2 instance role."
  level "High"
  objectives ["describe_internet_gateways"]
  audit_objects ["object.internet_gateways.internet_gateway_id"]
  operators ["=~"]
  raise_when [//]
  id_map ["object.internet_gateways.internet_gateway_id"]
  meta_rule_query <<~QUERY
  {
    gateways as var(func: <%= filter['internet_gateway'] %>) @cascade {
        <%= default_predicates %>
        relates_to @filter(<%= filter['route'] %>) {
          <%= default_predicates %>
          relates_to @filter(<%= filter['route_table'] %>) {
            <%= default_predicates %>
            relates_to @filter(<%= filter['route_table_association'] %>) {
              <%= default_predicates %>
              relates_to @filter(<%= filter['subnet'] %>) {
                <%= default_predicates %>
                pub_instances as relates_to @filter(<%= filter['instance'] %> AND has(public_ip_address)) {
                  <%= default_predicates %>
                  exposed_keys as relates_to @filter(<%= filter['key_pair'] %>){
                    <%= default_predicates %>
                    relates_to @filter(<%= filter['instance'] %> AND NOT uid(pub_instances)) {
                      <%= default_predicates %>
                      relates_to @filter(<%= filter['iam_instance_profile'] %>){
                        <%= default_predicates %>
                        relates_to @filter(<%= filter['role'] %>){
                          <%= default_predicates %>
                          exposed_policies as relates_to @filter(<%= filter['policy'] %>){
                            <%= default_predicates %>
                            exposed_policy_arns as policy_arn
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        
      }
    }
    query(func: uid(gateways)) @cascade {
      <%= default_predicates %>
      relates_to @filter(<%= filter['route'] %>) {
        <%= default_predicates %>
        relates_to @filter(<%= filter['route_table'] %>) {
          <%= default_predicates %>
          relates_to @filter(<%= filter['route_table_association'] %>) {
            <%= default_predicates %>
            relates_to @filter(<%= filter['subnet'] %>) {
              <%= default_predicates %>
              relates_to @filter(<%= filter['instance'] %>) {
                <%= default_predicates %>
                relates_to @filter(uid(exposed_keys)){
                  <%= default_predicates %>
                  relates_to @filter(<%= filter['instance'] %>){
                    <%= default_predicates %>
                    relates_to @filter(<%= filter['iam_instance_profile'] %>){
                      <%= default_predicates %>
                      relates_to @filter(<%= filter['role'] %>){
                        <%= default_predicates %>
                        relates_to @filter(uid(exposed_policies) AND eq(val(exposed_policy_arns), "arn:aws:iam::aws:policy/AdministratorAccess")){
                          <%= default_predicates %>
                          policy_name policy_arn                      
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers ({
      'internet_gateway' => ['relates_to'],
      'route' => [],
      'route_table' => [],
      'route_table_association' => [],
      'iam_instance_profile' => [],
      'role' => [],
      'policy' => [],
      'key_pair' => []
  })
end

coreo_aws_rule "instance-has-public-access-and-full-s3-privileges" do
  action :define
  service :ec2
  link "https://kb.securestate.vmware.com/aws-connected-threats-public-instance-with-full-s3-privileges"
  display_name "Publicly routable instance has full S3 iam privileges"
  description "A publicly accessible instance can make any changes to any S3 bucket or bucket ACLs."
  category "Security"
  suggested_action "Remove this privilege from any publicly accessable instance."
  level "High"
  objectives ["describe_internet_gateways"]
  audit_objects ["object.internet_gateways.internet_gateway_id"]
  operators ["=~"]
  raise_when [//]
  id_map ["object.internet_gateways.internet_gateway_id"]
  meta_rule_query <<~QUERY
  {
    gateways as var(func: <%= filter['internet_gateway'] %>) { }
    routes as var(func:  <%= filter['route'] %>) { }
    tables as var(func:  <%= filter['route_table'] %>) { }
    associations as var(func:  <%= filter['route_table_association'] %>) { }
    subnets as var(func:  <%= filter['subnet'] %>) { }
    pub_instances as var(func:  <%= filter['instance'] %>) @filter(has(public_ip_address)) { }
    profiles as var(func:  <%= filter['iam_instance_profile'] %>) { }
    roles as var(func:  <%= filter['role'] %>) { }
    policies as var(func:  <%= filter['policy'] %>) @cascade {
      policy_arns as policy_arn
    }
    s3_full_access_policy as q(func: uid(policies)) @filter(eq(val(policy_arns), "arn:aws:iam::aws:policy/AmazonS3FullAccess")) { }
    pub_instances_with_s3_policy as var(func: uid(pub_instances)) @cascade {
      filtered_profiles as relates_to @filter(uid(profiles)) {
        filtered_roles as relates_to @filter(uid(roles)) {
          filtered_policies as relates_to @filter(uid(s3_full_access_policy))
        }
      }
    }
    pub_instances_with_gateways as var(func: uid(pub_instances)) @cascade {
      filtered_subnets as relates_to @filter(uid(subnets)) {
        filtered_associations as relates_to @filter(uid(associations)) {
          filtered_tables as relates_to @filter(uid(tables)) {
            filtered_routes as relates_to @filter(uid(routes)) {
              filtered_gateways as relates_to @filter(uid(gateways))
            }
          }
        }
      }
    }
    query(func: uid(pub_instances_with_s3_policy)) @filter(uid(pub_instances_with_gateways)) {
      <%= default_predicates %>
      public_ip_address
      public_dns_name
      relates_to @filter(uid(filtered_profiles) OR uid(filtered_subnets)) {
        <%= default_predicates %>
        instance_profile_name
        cidr_block tags
        relates_to @filter(uid(filtered_roles) OR uid(filtered_associations)) {
          <%= default_predicates %>
          role_name
          description
          assume_role_policy_document
          implicit_route_association: main
          relates_to @filter(uid(filtered_policies) OR uid(filtered_tables)) {
            <%= default_predicates %>
            policy_arn
            relates_to @filter(uid(filtered_routes)) {
              <%= default_predicates %>
              relates_to @filter(uid(filtered_gateways)) {
                <%= default_predicates %>
              }
            }
          }
        }
      }
    }
  }
  QUERY
  meta_rule_node_triggers ({
      'internet_gateway' => [],
      'route' => [],
      'route_table' => [],
      'route_table_association' => [],
      'subnet' => [],
      'instance' => ['public_ip_address'],
      'iam_instance_profile' => [],
      'role' => [],
      'policy' => ['policy_arn']
  })
end
